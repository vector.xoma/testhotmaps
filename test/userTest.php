<?php

use PHPUnit\Framework\TestCase;

class userTest extends TestCase
{
    private $userController;

    public function setUp()
    {
        $this->userController = new UserController();
    }

    public function testUserAuth()
    {
        $userAuth = $this->userController->auth('test', '12345');
        $this->assertEquals("OK", $userAuth);
    }

    public function testGetUserInfo()
    {
        // тут по хорошему, проверять бы еще и состав обязательных полей в ответе
        $userInfo = $this->userController->getUserInfo('ivanov');
        $this->assertEquals("OK", $userInfo);
    }

    public function testUpdateUserInfo()
    {
        $updateUser = $this->userController->updateUserInfo(1);
        $this->assertEquals("OK", $updateUser);
    }

    public function tearDown() {
        $this->userController = null;
    }
}