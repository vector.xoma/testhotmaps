<?php

use connector\Client;

class UserController
{
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function auth($login, $password)
    {
        $result = null;

        $requestData = [
            'login' => $login,
            'pass' => $password
        ];

        $response = $this->client->get('/auth', $requestData);

        if ($response->getStatusCode() == 200) {
            $responseData = $response->json();
            if ($responseData->status == "OK"){
                User::setToken($responseData->token);
                $result = User::getToken();
            }
        }
        return $result;
    }

    public function getUserInfo($userName)
    {
        $requestData = [
            'token' => User::getToken()
        ];

        User::setUser($this->client->get("/get-user/{$userName}", $requestData));
        $userInfo = User::getUser();

        return $userInfo->status;
    }

    public function updateUserInfo($userId)
    {
        $userData = User::getUser();
        $requestData = [
            'active' => $userData->active,
            'blocked' => true,
            'name' => "Petr Petrovich",
            'permissions' => [
                [
                    "id" => 1,
                    "permission" => "comment"
                ]
            ]
        ];
        $responseData = ($this->client->post("/user/{$userId}/update?{User::getToken()}", $requestData))->json();

        return $responseData->status;
    }

}