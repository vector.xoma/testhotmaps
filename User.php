<?php


class User
{
    public static $user;

    /**
     * @param $userData
     */
    public function setUser($userData)
    {
        self::$user = (object)$userData;
    }

    /**
     * @return object User
     */
    public static function getUser()
    {
        return self::$user;
    }


    /**
     * @return string $token
     */
    public static function getToken()
    {
        return $_COOKIE['token'];
    }

    /**
     * @param $token string
     */
    public static function setToken($token)
    {
        setcookie('token', $token);
    }
}