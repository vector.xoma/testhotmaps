<?php
namespace connector;

use Exception;
use GuzzleHttp;

class Client
{
    private $client;

    public function __construct()
    {
        $this->client = new GuzzleHttp\Client(['base_uri' => $_ENV['HOST']]);
    }

    public function get($uri, $data)
    {
        try {
            return $this->client->request('GET', $uri, ['query' => $data]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function post($uri, $data)
    {
        try {
            return $this->client->request('POST', $uri, ['json' => $data]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function put($uri, $data)
    {
        //
    }

}